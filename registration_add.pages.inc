<?php
/**
 * @file
 */

function registration_add($js, $step = '') {

  $form_info = array(
    'id' => 'registration_add',
    'path' => 'admin/commerce/registrations/add/nojs/%step',
    'show trail' => TRUE,
    'show cancel' => TRUE,
    'show back' => TRUE,
    'show return' => FALSE,
    'next text' => t('Next'),
    'next callback' => 'registration_add_next',
    'finish text' => t('Save Registration'),
    'finish callback' => 'registration_add_finish',
    'order' => array(
      'entity' => t('Registration entity'),
      'type' => t('Registration type'),
      'author' => t('Registration owner'),
      'registration' => t('Registration'),
    ),
    'forms' => array(
      'entity' => array(
        'form id' => 'registration_add_entity_form',
	'title' => t('Registration entity'),
      ),
      'type' => array(
        'form id' => 'registration_add_type_form',
	'title' => t('Registration type'),
      ),
      'author' => array(
        'form id' => 'registration_add_author_form',
	'title' => t('Registration owner'),
      ),
      'registration' => array(
        'form id' => 'registration_add_form',
	'title' => t('Registration'),
      ),
    ),
  );

  ctools_include('object-cache');
  if (empty($step)) {
    // clear cache
    ctools_object_cache_clear('registration_add', 'registration_add');
    $step = 'entity';
  }
  else {
    // Get the object from cache.
    $object = registration_add_cache_get('registration_add');
  }

  $form_state = array(
    'object_id' => 'registration_add', // We need to set the object id for caching.
    'ajax' => $js, // We need to set this arbitrary key on the array.
    'modal' => NULL,
    'object' => NULL,
  );

  if (!isset($object)) {
    // Create a dummy object if we don't have one.
    $form_state['object'] = (object) array(
      'type' => NULL,
      'entity_id' => NULL,
      'entity_type' => NULL,
      'author' => NULL,
      'registration' => NULL,
    );
  }
  else {
    // Use the object that we already have.
    $form_state['object'] = &$object;
  }

  ctools_include('wizard');
  $form = ctools_wizard_multistep_form($form_info, $step, $form_state);
  $output = drupal_render($form);

  if ($form_state['complete']) {
    drupal_goto('admin/commerce/registration');
  }

  return $output;
}

function registration_add_next(&$form_state) {
  ctools_include('object-cache');
  ctools_object_cache_set('registration_add', $form_state['object_id'], $form_state['object']);
}

function registration_add_type_form($form, &$form_state) {
  $types = registration_get_types();
  $options = array();

  foreach ($types as $name => $type) {
    // Assemble select options for type.
    $options[$name] = check_plain($type->label);
  }

  $form['type'] = array(
    '#type' => 'select',
    '#title' => t('Registration type'),
    '#description' => t('Please select the registration type.'),
    '#options' => $options,
    '#required' => TRUE,
  );

  return $form;
}

function registration_add_type_form_submit($form, &$form_state) {
  $form_state['object']->type = $form_state['values']['type'];
}

function registration_add_entity_form($form, &$form_state) {

  // Fetch the available registration entities.
  $query = db_select('registration_entity', 're');
  $query
    ->condition('re.status', 1)
    ->fields('re');
  $res = $query->execute();

  $entities = array();
  foreach ($res as $rec) {
    if (isset($entities[$rec->entity_type])) {
      $entities[$rec->entity_type] = array();
    }
    $entities[$rec->entity_type][] = $rec->entity_id;
  }

  if (empty($entities)) {
    // Do not allow the form to proceed.
    $form_state['complete'] = TRUE;
    return $form;
  }

  $options = array();
  foreach ($entities as $entity_type => $ids) {
    $objects = entity_load($entity_type, $ids);

    foreach ($objects as $entity_id => $object) {
      // Setup an options list with the entity type and entity id as the key
      // and the entity label as the value.
      $options[$entity_type . '|' . $entity_id] = entity_label($entity_type, $object);
    }
  }

  $form['entity'] = array(
    '#type' => 'select',
    '#title' => t('Registration entity'),
    '#description' => t('Please select the registration entity to add a registration to.'),
    '#required' => TRUE,
    '#options' => $options,
  );

  return $form;
}

function registration_add_entity_form_submit($form, &$form_state) { 
  list($type, $id) = explode('|', $form_state['values']['entity']);
  $form_state['object']->entity_id = $id;
  $form_state['object']->entity_type = $type;
}

function registration_add_author_form($form, &$form_state) {

  $form['author'] = array(
    '#type' => 'textfield',
    '#title' => t('Registration owner'),
    '#description' => t('Please specify the user account that will own the registration.'),
    '#autocomplete_path' => 'user/autocomplete',
    '#required' => TRUE,
  );

  return $form;
}

function registration_add_author_form_submit($form, &$form_state) {
  $account = user_load_by_name($form_state['values']['author']);
  $form_state['object']->author = $account;
}

function registration_add_form($form, &$form_state) {

  $values = array( 
    'entity_type' => $form_state['object']->entity_type,
    'entity_id' => $form_state['object']->entity_id,
    'type' => $form_state['object']->type,
  );
  $registration = entity_get_controller('registration')->create($values);

  $form = registration_form($form, $form_state, $registration);
  $form['#validate'] = array('registration_form_validate');
  unset($form['actions']);

  return $form;
}

function registration_add_form_submit($form, &$form_state) {
  $registration = $form_state['registration'];

  $registration->author_uid = $form_state['object']->author->uid;
  $registration->count = $form_state['values']['count'];
  $registration->state = (!empty($form_state['values']['state']))
    ? $form_state['values']['state'] : NULL;

  switch ($form_state['values']['who_is_registering']) {
    case REGISTRATION_REGISTRANT_TYPE_ANON:
      $registration->anon_mail = $form_state['values']['anon_mail'];
      break;
    case REGISTRATION_REGISTRANT_TYPE_ME:
      global $user;
      $registration->user_uid = $user->uid;
      break;
    case REGISTRATION_REGISTRANT_TYPE_USER:
      if ($reg_user = user_load_by_name($form_state['values']['user'])) {
        $registration->user_uid = $reg_user->uid;
      }   
      break;
  }

  // Notify field widgets.
  field_attach_submit('registration', $registration, $form, $form_state);

  $form_state['object']->registration = $registration;
}

function registration_add_finish(&$form_state) {
  $registration = $form_state['object']->registration;

  if (registration_save($registration)) {
    drupal_set_message(t('Registration added for %name.', array('%name' => $form_state['object']->author->name)));
  }
  else {
    drupal_set_message(t('There was an error trying to save the registration.'), 'error');
  }

  $form_state['redirect'] = 'admin/commerce/registration';

  $form_state['complete'] = TRUE;
}
